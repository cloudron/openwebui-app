[0.1.0]
* Initial version for Open WebUI

[0.2.0]
* Basic ollama integration

[0.3.0]
* Make local ollama optional

[0.4.0]
* Update OpenWebUI to 0.1.111
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.1.111)
* Model Whitelisting: Admins now have the ability to whitelist models for users with the 'user' role.
* Update All Models: Added a convenient button to update all models at once.
* Toggle PDF OCR: Users can now toggle PDF OCR option for improved parsing performance.
* DALL-E Integration: Introduced DALL-E integration for image generation alongside automatic1111.
* RAG API Refactoring: Refactored RAG logic and exposed its API, with additional documentation to follow.

[0.5.0]
* Update OpenWebUI to 0.1.113
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.1.113)
* Localization: You can now change the UI language in Settings > General. We support Ukrainian, German, Farsi (Persian), Traditional and Simplified Chinese and French translations. You can help us to translate the UI into your language! More info in our CONTRIBUTION.md.
* System-wide Theme: Introducing a new system-wide theme for enhanced visual experience.

[0.6.0]
* Update OpenWebUI to 0.1.114
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.1.114)
* Webhook Integration: Now you can subscribe to new user sign-up events via webhook. Simply navigate to the admin panel > admin settings > webhook URL.
* Enhanced Model Filtering: Alongside Ollama, OpenAI proxy model whitelisting, we've added model filtering functionality for LiteLLM proxy.
* Expanded Language Support: Spanish, Catalan, and Vietnamese languages are now available, with improvements made to others.
* Input Field Spelling: Resolved issue with spelling mistakes in input fields.
* Light Mode Styling: Fixed styling issue with light mode in document adding.
* Language Sorting: Languages are now sorted alphabetically by their code for improved organization.

[0.7.0]
* Update OpenWebUI to 0.1.115
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.1.115)
* Custom Model Selector: Easily find and select custom models with the new search filter feature.
* Cancel Model Download: Added the ability to cancel model downloads.
* Image Generation ComfyUI: Image generation now supports ComfyUI.
* Updated Light Theme: Updated the light theme for a fresh look.
* Additional Language Support: Now supporting Bulgarian, Italian, Portuguese, Japanese, and Dutch.

[0.8.0]
* Update OpenWebUI to 0.1.117
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.1.117)

[0.9.0]
* Update OpenWebUI to 0.1.118
* Update Ollama to 0.1.31
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.1.118)
* Ollama and CUDA Images: Added support for ':ollama' and ':cuda' tagged images.
* Enhanced Response Rating: Now you can annotate your ratings for better feedback.
* User Initials Profile Photo: User initials are now the default profile photo.
* Update RAG Embedding Model: Customize RAG embedding model directly in document settings.
* Additional Language Support: Added Turkish language support.

[0.10.0]
* Update OpenWebUI to 0.1.119
* Update Ollama to 0.1.32
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.1.119)
* Enhanced RAG Embedding Support: Ollama, and OpenAI models can now be used for RAG embedding model.
* Seamless Integration: Copy 'ollama run ' directly from Ollama page to easily select and pull models.
* Tagging Feature: Add tags to chats directly via the sidebar chat menu.
* Mobile Accessibility: Swipe left and right on mobile to effortlessly open and close the sidebar.
* Improved Navigation: Admin panel now supports pagination for user list.
* Additional Language Support: Added Polish language support.

[0.11.0]
* Update OpenWebUI to 0.1.120
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.1.120)
* Archive Chat Feature: Easily archive chats with a new sidebar button, and access archived chats via the profile button > archived chats.
* Configurable Text-to-Speech Endpoint: Customize your Text-to-Speech experience with configurable OpenAI endpoints.
* Improved Error Handling: Enhanced error message handling for connection failures.
* nhanced Shortcut: When editing messages, use ctrl/cmd+enter to save and submit, and esc to close.
* Language Support: Added support for Georgian and enhanced translations for Portuguese and Vietnamese.
* Model Selector: Resolved issue where default model selection was not saving.
* Share Link Copy Button: Fixed bug where the copy button wasn't copying links in Safari.
* Light Theme Styling: Addressed styling issue with the light theme.

[0.11.1]
* Update OpenWebUI to 0.1.121
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.1.121)
* Translation Issues: Addressed various translation discrepancies.
* LiteLLM Security Fix: Updated LiteLLM version to resolve a security vulnerability.
* HTML Tag Display: Rectified the issue where the '< br >' tag wasn't displaying correctly.
* WebSocket Connection: Resolved the failure of WebSocket connection under HTTPS security for ComfyUI server.
* FileReader Optimization: Implemented FileReader initialization per image in multi-file drag & drop to ensure reusability.
* Tag Display: Corrected tag display inconsistencies.
* Archived Chat Styling: Fixed styling issues in archived chat.
* Safari Copy Button Bug: Addressed the bug where the copy button failed to copy links in Safari.

[0.11.2]
* Update OpenWebUI to 0.1.122
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.1.122)
* Enhanced RAG Pipeline: Now with hybrid searching via 'BM25', reranking powered by 'CrossEncoder', and configurable relevance score thresholds.
* External Database Support: Seamlessly connect to custom SQLite or Postgres databases using the 'DATABASE_URL' environment variable.
* Remote ChromaDB Support: Introducing the capability to connect to remote ChromaDB servers.
* Improved Admin Panel: Admins can now conveniently check users' chat lists and last active status directly from the admin panel.
* Splash Screen: Introducing a loading splash screen for a smoother user experience.
* Language Support Expansion: Added support for Bangla (bn-BD), along with enhancements to Chinese, Spanish, and Ukrainian translations.
* Improved LaTeX Rendering Performance: Enjoy faster rendering times for LaTeX equations.
* More Environment Variables: Explore additional environment variables in our documentation (https://docs.openwebui.com), including the 'ENABLE_LITELLM' option to manage memory usage.
* Ollama Compatibility: Resolved errors occurring when Ollama server version isn't an integer, such as SHA builds or RCs.
* Various OpenAI API Issues: Addressed several issues related to the OpenAI API.
* Stop Sequence Issue: Fixed the problem where the stop sequence with a backslash '' was not functioning.
* Font Fallback: Corrected font fallback issue.
* Prompt Input Behavior on Mobile: Enter key prompt submission disabled on mobile devices for improved user experience.

[0.11.3]
* Fix display of favicon

[0.11.4]
* Update OpenWebUI to 0.1.123
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.1.123)
* New Landing Page Design: Refreshed design for a more modern look and optimized use of screen space.
* Youtube RAG Pipeline: Introduces dedicated RAG pipeline for Youtube videos, enabling interaction with video transcriptions directly.
* Enhanced Admin Panel: Streamlined user management with options to add users directly or in bulk via CSV import.
* '@' Model Integration: Easily switch to specific models during conversations; old collaborative chat feature phased out.
* Language Enhancements: Swedish translation added, plus improvements to German, Spanish, and the addition of Doge translation.

[0.11.5]
* Update OpenWebUI to 0.1.124
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.1.124)
* Improved Chat Sidebar: Now conveniently displays time ranges and organizes chats by today, yesterday, and more.
* Citations in RAG Feature: Easily track the context fed to the LLM with added citations in the RAG feature.
* Auth Disable Option: Introducing the ability to disable authentication. Set 'WEBUI_AUTH' to False to disable authentication. Note: Only applicable for fresh installations without existing users.
* Enhanced YouTube RAG Pipeline: Now supports non-English videos for an enriched experience.
* Specify OpenAI TTS Models: Customize your TTS experience by specifying OpenAI TTS models.
* Additional Environment Variables: Discover more environment variables in our comprehensive documentation at Open WebUI Documentation (https://docs.openwebui.com).
* Language Support: Arabic, Finnish, and Hindi added; Improved support for German, Vietnamese, and Chinese.
* Model Selector Styling: Addressed styling issues for improved user experience.
* Warning Messages: Resolved backend warning messages.
* Title Generation: Limited output to 50 tokens.

[0.11.6]
* Update OpenWebUI to 0.1.125
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.1.125)
* Updated UI: Chat interface revamped with chat bubbles. Easily switch back to the old style via settings > interface > chat bubble UI.
* Enhanced Sidebar UI: Model files, documents, prompts, and playground merged into Workspace for streamlined access.
* Improved Many Model Interaction: All responses now displayed simultaneously for a smoother experience.
* Python Code Execution: Execute Python code locally in the browser with libraries like 'requests', 'beautifulsoup4', 'numpy', 'pandas', 'seaborn', 'matplotlib', 'scikit-learn', 'scipy', 'regex'.
* Experimental Memory Feature: Manually input personal information you want LLMs to remember via settings > personalization > memory.
* Persistent Settings: Settings now saved as config.json for convenience.
* Health Check Endpoint: Added for Docker deployment.
* TL Support: Toggle chat direction via settings > interface > chat direction.
* PowerPoint Support: RAG pipeline now supports PowerPoint documents.
* Language Updates: Ukrainian, Turkish, Arabic, Chinese, Serbian, Vietnamese updated; Punjabi added.
* Shared Chat Update: Shared chat now includes creator user information.

[1.0.0]
* **SQLITE DEPRECATION NOTICE** - This package is moving from sqlite to PostgreSQL .
  * Data is not migrated and all data will get reset.
  * We recommend simply reinstalling the app to use PostgreSQL. 
  * [Migration Script](https://git.cloudron.io/cloudron/openwebui-app/-/merge_requests/1#note_12571)

[2.0.0]
* **SQLITE DEPRECATION NOTICE** - This package has moved from sqlite to PostgreSQL
  * Data is not migrated and all data will get reset.
  * We recommend simply reinstalling the app to use PostgreSQL. 
  * [Untested Migration Script](https://git.cloudron.io/cloudron/openwebui-app/-/merge_requests/1#note_12571)

[2.1.0]
* Update OpenWebUI to 0.2.2
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.2.0)
* Pipelines Support: Open WebUI now includes a plugin framework for enhanced customization and functionality (https://github.com/open-webui/pipelines). Easily add custom logic and integrate Python libraries, from AI agents to home automation APIs.
* Function Calling via Pipelines: Integrate function calling seamlessly through Pipelines.
* ser Rate Limiting via Pipelines: Implement user-specific rate limits to manage API usage efficiently.
* Usage Monitoring with Langfuse: Track and analyze usage statistics with Langfuse integration through Pipelines.
* Conversation Turn Limits: Set limits on conversation turns to manage interactions better through Pipelines.
* Toxic Message Filtering: Automatically filter out toxic messages to maintain a safe environment using Pipelines.

[2.1.1]
* Update OpenWebUI to 0.2.4
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.2.4)
* Improved Account Pending Page: The account pending page now displays admin details by default to avoid confusion. You can disable this feature in the admin settings if needed.
* HTTP Proxy Support: We have enabled the use of the 'http_proxy' environment variable in OpenAI and Ollama API calls, making it easier to configure network settings.
* Quick Access to Documentation: You can now easily access Open WebUI documents via a question mark button located at the bottom right corner of the screen (available on larger screens like PCs).
* Enhanced Translation: Improvements have been made to translations.

[2.1.2]
* Update OpenWebUI to 0.2.5
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.2.5)
* Active Users Indicator: Now you can see how many people are currently active and what they are running. This helps you gauge when performance might slow down due to a high number of users.
* Create Ollama Modelfile: The option to create a modelfile for Ollama has been reintroduced in the Settings > Models section, making it easier to manage your models.
* Default Model Setting: Added an option to set the default model from Settings > Interface. This feature is now easily accessible, especially convenient for mobile users as it was previously hidden.
* Enhanced Translations: We've improved the Chinese translations and added support for Turkmen and Norwegian languages to make the interface more accessible globally.
* Mobile View Improvements: The UI now uses dvh (dynamic viewport height) instead of vh (viewport height), providing a better and more responsive experience for mobile users.

[2.2.0]
* Update OpenWebUI to 0.3.1
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.3.1)

[2.2.1]
* Update OpenWebUI to 0.3.2
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.3.2)
* Web Search Query Status: The web search query will now persist in the results section to aid in easier debugging and tracking of search queries.
* New Web Search Provider: We have added Serply as a new option for web search providers, giving you more choices for your search needs.
* Improved Translations: We've enhanced translations for Chinese and Portuguese.

[2.2.2]
* Update OpenWebUI to 0.3.3
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.3.3)

[2.2.3]
* Update OpenWebUI to 0.3.4
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.3.4)

[2.2.4]
* Update OpenWebUI to 0.3.5
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.3.5)

[2.2.5]
* Set `NLTK_DIR` env var

[2.2.6]
* Update OpenWebUI to 0.3.6
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.3.6)

[2.2.7]
* Update OpenWebUI to 0.3.7
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.3.7)

[2.3.0]
* Update OpenWebUI to 0.3.8
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.3.8)
* Update Ollama to 0.2.1

[2.3.1]
* Update Ollama to 0.2.2

[2.3.2]
* Update Ollama to 0.2.5

[2.3.3]
* Update OpenWebUI to 0.3.9
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.3.9)

[2.3.4]
* Update OpenWebUI to 0.3.10
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.3.10)

[2.3.5]
* Update Ollama to 0.2.6

[2.3.6]
* Update Ollama to 0.2.7

[2.3.7]
* Update Ollama to 0.2.8

[2.3.8]
* Update Ollama to 0.3.0

[2.3.9]
* Update Ollama to 0.3.1

[2.3.10]
* Update OpenWebUI to 0.3.11
* Update Ollama to 0.3.3
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.3.11)

[2.3.11]
* Update OpenWebUI to 0.3.12
* Update Ollama to 0.3.4
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.3.12)

[2.3.12]
* Update Ollama to 0.3.5

[2.3.13]
* Update Ollama to 0.3.6

[2.3.14]
* Update OpenWebUI to 0.3.13
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.3.13)
* Enhanced Markdown Rendering: Significant improvements in rendering markdown, ensuring smooth and reliable display of LaTeX and Mermaid charts, enhancing user experience with more robust visual content.
* Auto-Install Tools & Functions Python Dependencies: For 'Tools' and 'Functions', Open WebUI now automatically install extra python requirements specified in the frontmatter, streamlining setup processes and customization.
* OAuth Email Claim Customization: Introduced an `OAUTH_EMAIL_CLAIM` variable to allow customization of the default "email" claim within OAuth configurations, providing greater flexibility in authentication processes.
* Websocket Reconnection: Enhanced reliability with the capability to automatically reconnect when a websocket is closed, ensuring consistent and stable communication.
* Haptic Feedback on Support Devices: Android devices now support haptic feedback for an immersive tactile experience during certain interactions.

[2.3.15]
* Update OpenWebUI to 0.3.15
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.3.15)

[2.3.16]
* Update OpenWebUI to 0.3.16
* Update Ollama to 0.3.8
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.3.15)

[2.4.0]
* Update Ollama to 0.3.9
* Introduce Cloudron OpenID support for new installations

[2.4.1]
* Update OpenWebUI to 0.3.18
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.3.18)

[2.4.2]
* Update OpenWebUI to 0.3.19
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.3.19)

[2.4.3]
* Update OpenWebUI to 0.3.20
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.3.20)
* Document Count Display: Now displays the total number of documents directly within the dashboard.
* Ollama Embed API Endpoint: Enabled /api/embed endpoint proxy support.
* Docker Launch Issue: Resolved the problem preventing Open-WebUI from launching correctly when using Docker.
* Enhanced Search Prompts: Improved the search query generation prompts for better accuracy and user interaction, enhancing the overall search experience.

[2.5.0]
* Fix RAG setup

[2.5.1]
* Update OpenWebUI to 0.3.22
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.3.22)

[2.5.2]
* Update OpenWebUI to 0.3.23
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.3.23)
* WebSocket Redis Support: Enhanced load balancing capabilities for multiple instance setups, promoting better performance and reliability in WebUI.
* Adjustable Chat Controls: Introduced width-adjustable chat controls, enabling a personalized and more comfortable user interface.
* i18n Updates: Improved and updated the Chinese translations.

[2.5.3]
* Update OpenWebUI to 0.3.26
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.3.26)

[2.5.4]
* Update OpenWebUI to 0.3.28
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.3.28)

[2.5.5]
* Update OpenWebUI to 0.3.29
* Update Ollama to 0.3.12
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.3.29)
* Fix nltk punkt_tab issue

[2.5.6]
* Update OpenWebUI to 0.3.30
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.3.30)
* Update Available Toast Dismissal: Enhanced user experience by ensuring that once the update available notification is dismissed, it won't reappear for 24 hours.
* Ollama /embed Form Data: Adjusted the integration inaccuracies in the /embed form data to ensure it perfectly matches with Ollama's specifications.
* O1 Max Completion Tokens Issue: Resolved compatibility issues with OpenAI's o1 models max_completion_tokens param to ensure smooth operation.
* Pip Install Database Issue: Fixed a critical issue where database changes during pip installations were reverting and not saving chat logs, now ensuring data persistence and reliability in chat operations.
* Chat Rename Tab Update: Fixed the functionality to change the web browser's tab title simultaneously when a chat is renamed, keeping tab titles consistent.

[2.5.7]
* Update OpenWebUI to 0.3.32
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.3.32)

[2.5.8]
* Update Ollama to 0.3.13

[2.5.9]
* Update Ollama to 0.3.14

[2.5.10]
* Update OpenWebUI to 0.3.35
* [Full changelog](https://github.com/open-webui/open-webui/releases/tag/v0.3.35)

[2.6.0]
* Update Open WebUI to 0.4.0
* Support for Llama 3.2 Vision (i.e. Mllama) architecture
* Sending follow on requests to vision models will now be much faster
* Fixed issues where stop sequences would not be detected correctly
* Ollama can now import models from Safetensors without a Modelfile when running `ollama create my-model`
* Fixed issue where redirecting output to a file on Windows would cause invalid characters to be written
* Fixed issue where invalid model data would cause Ollama to error

[2.6.1]
* Update ollama to 0.4.1

[2.6.2]
* Update ollama to 0.4.2

[2.7.0]
* Update open-webui to 0.4.0
* [Full Changelog](https://github.com/open-webui/open-webui/releases/tag/v0.4.0)
* **User Groups**: You can now create and manage user groups, making user organization seamless.
* **Group-Based Access Control**: Set granular access to models, knowledge, prompts, and tools based on user groups, allowing for more controlled and secure environments.
* **Group-Based User Permissions**: Easily manage workspace permissions. Grant users the ability to upload files, delete, edit, or create temporary chats, as well as define their ability to create models, knowledge, prompts, and tools.
* **LDAP Support**: Newly introduced LDAP authentication adds robust security and scalability to user management.
* **Enhanced OpenAI-Compatible Connections**: Added prefix ID support to avoid model ID clashes, with explicit model ID support for APIs lacking '/models' endpoint support, ensuring smooth operation with custom setups.

[2.7.1]
* Update open-webui to 0.4.1
* [Full Changelog](https://github.com/open-webui/open-webui/releases/tag/v0.4.1)
* **Enhanced Feedback System**: Introduced a detailed 1-10 rating scale for feedback alongside thumbs up/down, preparing for more precise model fine-tuning and improving feedback quality.
* **Tool Descriptions on Hover**: Easily access tool descriptions by hovering over the message input, providing a smoother workflow with more context when utilizing tools.

[2.7.2]
* Update open-webui to 0.4.2
* [Full Changelog](https://github.com/open-webui/open-webui/releases/tag/v0.4.2)
* **Knowledge Files Visibility Issue**: Resolved the bug preventing individual files in knowledge collections from displaying when referenced with '#'.
* **OpenAI Endpoint Prefix**: Fixed the issue where certain OpenAI connections that deviate from the official API spec werent working correctly with prefixes.
* **Arena Model Access Control**: Corrected an issue where arena model access control settings were not being saved.
* **Usage Capability Selector**: Fixed the broken usage capabilities selector in the model editor.

[2.7.3]
* Update ollama to 0.4.3
* [Full Changelog](https://github.com/open-webui/open-webui/releases/tag/v0.4.3)
* [Tlu 3](https://ollama.com/library/tulu3): Tlu 3 is a leading instruction following model family, offering fully open-source data, code, and recipes by the The Allen Institute for AI.
* [Mistral Large](https://ollama.com/library/mistral-large): a new version of Mistral Large with improved Long Context, Function Calling and System Prompt support.
* Fixed crash that occurred on macOS devices with low memory
* Improved performance issues that occurred in Ollama versions 0.4.0-0.4.2
* Fixed issue that would cause `granite3-dense` to generate empty responses
* Fixed crashes and hanging caused by KV cache management

[2.7.4]
* Update open-webui to 0.4.4
* [Full Changelog](https://github.com/open-webui/open-webui/releases/tag/v0.4.4)
* **Translation Updates**: Refreshed Catalan, Brazilian Portuguese, German, and Ukrainian translations, further enhancing the platform's accessibility and improving the experience for international users.
* **Mobile Controls Visibility**: Resolved an issue where the controls button was not displaying on the new chats page for mobile users, ensuring smoother navigation and functionality on smaller screens.
* **RAG Query Generation Issue**: Addressed a significant problem where RAG query generation occurred unnecessarily without attached files, drastically improving speed and reducing delays during chat completions.
* **Legacy Event Emitter Support**: Reintroduced compatibility with legacy "citation" types for event emitters in tools and functions, providing smoother workflows and broader tool support for users.

[2.7.5]
* Update ollama to 0.4.4

[2.7.6]
* Update ollama to 0.4.5

[2.7.7]
* Update open-webui to 0.4.5
* [Full Changelog](https://github.com/open-webui/open-webui/releases/tag/v0.4.5)
* ** Model Order/Defaults Reintroduced**: Brought back the ability to set model order and default models, now configurable via Admin Settings > Models > Configure (Gear Icon).
* ** Query Generation Issue**: Resolved an error in web search query generation, enhancing search accuracy and ensuring smoother search workflows.
* ** Textarea Auto Height Bug**: Fixed a layout issue where textarea input height was shifting unpredictably, particularly when editing system prompts.
* ** Ollama Authentication**: Corrected an issue with Ollamas authorization headers, guaranteeing reliable authentication across all endpoints.
* ** Missing Min_P Save**: Resolved an issue where the 'min_p' parameter was not being saved in configurations.
* ** Tools Description**: Fixed a key issue that omitted tool descriptions in tools payload.

[2.7.8]
* Update open-webui to 0.4.6
* [Full Changelog](https://github.com/open-webui/open-webui/releases/tag/v0.4.6)
* ** Enhanced Translations**: Various language translations improved to make the WebUI more accessible and user-friendly worldwide.
* ** Textarea Shifting Bug**: Resolved the issue where the textarea shifted unexpectedly, ensuring a smoother typing experience.
* ** Model Configuration Modal**: Fixed the issue where the models configuration modal introduced in 0.4.5 wasnt working for some users.
* ** Legacy Query Support**: Restored functionality for custom query generation in RAG when using legacy prompts, ensuring both default and custom templates now work seamlessly.
* ** Improved General Reliability**: Various minor fixes improve platform stability and ensure a smoother overall experience across workflows.

[2.7.9]
* Update open-webui to 0.4.7
* [Full Changelog](https://github.com/open-webui/open-webui/releases/tag/v0.4.7)
* **Prompt Input Auto-Completion**: Type a prompt and let AI intelligently suggest and complete your inputs. Simply press 'Tab' or swipe right on mobile to confirm. Available only with Rich Text Input (default setting). Disable via Admin Settings for full control.
* **Improved Translations**: Enhanced localization for multiple languages, ensuring a more polished and accessible experience for international users.
* **Tools Export Issue**: Resolved a critical issue where exporting tools wasnt functioning, restoring seamless export capabilities.
* **Model ID Registration**: Fixed an issue where model IDs werent registering correctly in the model editor, ensuring reliable model setup and tracking.
* **Textarea Auto-Expansion**: Corrected a bug where textareas didnt expand automatically on certain browsers, improving usability for multi-line inputs.

[2.8.0]
* Update ollama to 0.5.1
* [Full Changelog](https://github.com/open-webui/open-webui/releases/tag/v0.5.1)
* Fixed issue where Ollama's API would generate JSON output when specifying `"format": null`
* Fixed issue where passing `--format json` to `ollama run` would cause an error
* [Llama 3.3](https://ollama.com/library/llama3.3): a new state of the art 70B model. Llama 3.3 70B offers similar performance compared to Llama 3.1 405B model.
* [Snowflake Arctic Embed 2](https://ollama.com/library/snowflake-arctic-embed2): Snowflake's frontier embedding model. Arctic Embed 2.0 adds multilingual support without sacrificing English performance or scalability.
* Fixed error importing model vocabulary files

[2.8.1]
* Update open-webui to 0.4.8
* [Full Changelog](https://github.com/open-webui/open-webui/releases/tag/v0.5.1)
* **Bypass Model Access Control**: Introduced the 'BYPASS_MODEL_ACCESS_CONTROL' environment variable. Easily bypass model access controls for user roles when access control isn't required, simplifying workflows for trusted environments.
* **Markdown in Banners**: Now supports markdown for banners, enabling richer, more visually engaging announcements.
* **Internationalization Updates**: Enhanced translations across multiple languages, further improving accessibility and global user experience.
* **Styling Enhancements**: General UI style refinements for a cleaner and more polished interface.
* **Rich Text Reliability**: Improved the reliability and stability of rich text input across chats for smoother interactions.
* **Tailwind Build Issue**: Resolved a breaking bug caused by Tailwind, ensuring smoother builds and overall system reliability.
* **Knowledge Collection Query Fix**: Addressed API endpoint issues with querying knowledge collections, ensuring accurate and reliable information retrieval.

[2.8.2]
* Update ollama to 0.5.3
* [Full Changelog](https://github.com/open-webui/open-webui/releases/tag/v0.5.3)
* Fixed runtime errors on older Intel Macs
* Fixed issue where setting the `format` field to `""` would cause an error
* [@&#8203;Askir](https://github.com/Askir) made their first contribution in https://github.com/ollama/ollama/pull/8028
* [EXAONE 3.5](https://ollama.com/library/exaone3.5): a collection of instruction-tuned bilingual (English and Korean) generative models ranging from 2.4B to 32B parameters, developed and released by LG AI Research.
* Fixed issue where whitespace would get trimmed from prompt when images were provided
* Improved memory estimation when scheduling models

[2.8.3]
* Update ollama to 0.5.4
* [Full Changelog](https://github.com/open-webui/open-webui/releases/tag/v0.5.4)
* [Falcon3](https://ollama.com/library/falcon3): A family of efficient AI models under 10B parameters performant in science, math, and coding through innovative training techniques.
* Fixed issue where providing `null` to `format` would result in an error

[2.9.0]
* CLOUDRON_OIDC_PROVIDER_NAME implemented
* checklist added

[2.9.1]
* Add storage info to checklist
[2.10.0]
* Update open-webui to 0.5.0
* [Full Changelog](https://github.com/open-webui/open-webui/releases/tag/v0.5.4)
* ** True Asynchronous Chat Support**: Create chats, navigate away, and return anytime with responses ready. Ideal for reasoning models and multi-agent workflows, enhancing multitasking like never before.
* ** Chat Completion Notifications**: Never miss a completed response. Receive instant in-UI notifications when a chat finishes in a non-active tab, keeping you updated while you work elsewhere.
* ** Notification Webhook Integration**: Get alerts via webhooks even when your tab is closed! Configure your webhook URL in Settings > Account and receive timely updates for long-running chats or external integration needs.
* ** Channels (Beta)**: Explore Discord/Slack-style chat rooms designed for real-time collaboration between users and AIs. Build bots for channels and unlock asynchronous communication for proactive multi-agent workflows. Opt-in via Admin Settings > General. A Comprehensive Bot SDK tutorial (https://github.com/open-webui/bot) is incoming, so stay tuned!
* ** Client-Side Image Compression**: Now compress images before upload (Settings > Interface), saving bandwidth and improving performance seamlessly.
* ** OAuth Management for User Groups**: Enable group-level management via OAuth integration for enhanced control and scalability in collaborative environments.
* ** Structured Output for Ollama**: Pass structured data output directly to Ollama, unlocking new possibilities for streamlined automation and precise data handling.
* ** Offline Swagger Documentation**: Developer-friendly Swagger API docs are now available offline, ensuring full accessibility wherever you are.
* ** Quick Screen Capture Button**: Effortlessly capture your screen with a single click from the message input menu.
* ** i18n Updates**: Improved and refined translations across several languages, including Ukrainian, German, Brazilian Portuguese, Catalan, and more, ensuring a seamless global user experience.
* ** Table Export to CSV**: Resolved issues with CSV export where headers were missing or errors occurred due to values with commas, ensuring smooth and reliable data handling.
* ** BYPASS_MODEL_ACCESS_CONTROL**: Fixed an issue where users could see models but couldnt use them with 'BYPASS_MODEL_ACCESS_CONTROL=True', restoring proper functionality for environments leveraging this setting.
* ** API Key Authentication Restriction**: Narrowed API key auth permissions to '/api/models' and '/api/chat/completions' for enhanced security and better API governance.
* ** Backend Overhaul for Performance**: Major backend restructuring; a heads-up that some "Functions" using internal variables may face compatibility issues. Moving forward, websocket support is mandatory to ensure Open WebUI operates seamlessly.
* ** Legacy Functionality Clean-Up**: Deprecated outdated backend systems that were non-essential or overlapped with newer implementations, allowing for a leaner, more efficient platform.

[2.10.1]
* Update open-webui to 0.5.1
* [Full Changelog](https://github.com/open-webui/open-webui/releases/tag/v0.5.1)
* Notification Sound Toggle: Added a new setting under Settings > Interface to disable notification sounds, giving you greater control over your workspace environment and focus.
* Non-Streaming Response Visibility: Resolved an issue where non-streaming responses were not displayed, ensuring all responses are now reliably shown in your conversations.
* Title Generation with OpenAI APIs: Fixed a bug preventing title generation when using OpenAI APIs, restoring the ability to automatically generate chat titles for smoother organization.
* Admin Panel User List: Addressed the issue where only 50 users were visible in the admin panel. You can now manage and view all users without restrictions.
* Image Generation Error: Fixed the issue causing 'get_automatic1111\_api_auth()' errors in image generation, ensuring seamless creative workflows.
* Pipeline Settings Loading Issue: Resolved a problem where pipeline settings were stuck at the loading screen, restoring full configurability in the admin panel.

[2.10.2]
* Update open-webui to 0.5.2
* [Full Changelog](https://github.com/open-webui/open-webui/releases/tag/v0.5.2)
* **Typing Indicators in Channels**: Know exactly whos typing in real-time within your channels, enhancing collaboration and keeping everyone engaged.
* **User Status Indicators**: Quickly view a users status by clicking their profile image in channels for better coordination and availability insights.
* **Configurable API Key Authentication Restrictions**: Flexibly configure endpoint restrictions for API key authentication, now off by default for a smoother setup in trusted environments.
* **Playground Functionality Restored**: Resolved a critical issue where the playground wasnt working, ensuring seamless experimentation and troubleshooting workflows.
* **Corrected Ollama Usage Statistics**: Fixed a calculation error in Ollamas usage statistics, providing more accurate tracking and insights for better resource management.
* **Pipelines Outlet Hook Registration**: Addressed an issue where outlet hooks for pipelines werent registered, restoring functionality and consistency in pipeline workflows.
* **Image Generation Error**: Resolved a persistent issue causing errors with 'get_automatic1111\_api_auth()' to ensure smooth image generation workflows.
* **Text-to-Speech Error**: Fixed the missing argument in Eleven Labs 'get_available_voices()', restoring full text-to-speech capabilities for uninterrupted voice interactions.
* **Title Generation Issue**: Fixed a bug where title generation was not working in certain cases, ensuring consistent and reliable chat organization.

[2.10.3]
* Update open-webui to 0.5.3
* [Full Changelog](https://github.com/open-webui/open-webui/releases/tag/v0.5.3)
* ** Channel Reactions with Built-In Emoji Picker**: Easily express yourself in channel threads and messages with reactions, featuring an intuitive built-in emoji picker for seamless selection.
* ** Threads for Channels**: Organize discussions within channels by creating threads, improving clarity and fostering focused conversations.
* ** Reset Button for SVG Pan/Zoom**: Added a handy reset button to SVG Pan/Zoom, allowing users to quickly return diagrams or visuals to their default state without hassle.
* ** Realtime Chat Save Environment Variable**: Introduced the ENABLE_REALTIME_CHAT_SAVE environment variable. Choose between faster responses by disabling realtime chat saving or ensuring chunk-by-chunk data persistency for critical operations.

[2.10.4]
* Update open-webui to 0.5.4
* [Full Changelog](https://github.com/open-webui/open-webui/releases/tag/v0.5.4)
* ** Clone Shared Chats**: Effortlessly clone shared chats to save time and streamline collaboration, perfect for reusing insightful discussions or custom setups.
* ** Native Notifications for Channel Messages**: Stay informed with integrated desktop notifications for channel messages, ensuring you never miss important updates while multitasking.
* ** Torch MPS Support**: MPS support for Mac users when Open WebUI is installed directly, offering better performance and compatibility for AI workloads.
* ** Enhanced Translations**: Small improvements to various translations, ensuring a smoother global user experience.
* ** Image-Only Messages in Channels**: You can now send images without accompanying text or content in channels.
* ** Proper Exception Handling**: Enhanced error feedback by ensuring exceptions are raised clearly, reducing confusion and promoting smoother debugging.
* ** RAG Query Generation Restored**: Fixed query generation issues for Retrieval-Augmented Generation, improving retrieval accuracy and ensuring seamless functionality.
* ** MOA Response Functionality Fixed**: Addressed an error with the MOA response generation feature.
* ** Channel Thread Loading with 50+ Messages**: Resolved an issue where channel threads stalled when exceeding 50 messages, ensuring smooth navigation in active discussions.
* ** API Endpoint Restrictions Resolution**: Fixed a critical bug where the 'API_KEY_ALLOWED_ENDPOINTS' setting was not functioning as intended, ensuring API access is limited to specified endpoints for enhanced security.
* ** Action Functions Restored**: Corrected an issue preventing action functions from working, restoring their utility for customized automations and workflows.
* ** Temporary Chat JSON Export Fix**: Resolved a bug blocking temporary chats from being exported in JSON format, ensuring seamless data portability.
* ** Sidebar UI Tweaks**: Chat folders, including pinned folders, now display below the Chats section for better organization; the "New Folder" button has been relocated to the Chats section for a more intuitive workflow.
* ** Real-Time Save Disabled by Default**: The 'ENABLE_REALTIME_CHAT_SAVE' setting is now off by default, boosting response speed for users who prioritize performance in high-paced workflows or less critical scenarios.
* ** Audio Input Echo Cancellation**: Audio input now features echo cancellation enabled by default, reducing audio feedback for improved clarity during conversations or voice-based interactions.
* ** General Reliability Improvements**: Numerous under-the-hood enhancements have been made to improve platform stability, boost overall performance, and ensure a more seamless, dependable experience across workflows.

[2.10.5]
* Update ollama to 0.5.5
* [Phi-4](https://ollama.com/library/phi4): Phi 4 is a 14B parameter, state-of-the-art open model from Microsoft.
* [Command R7B](https://ollama.com/library/command-r7b): the smallest model in Cohere's R series delivers top-tier speed, efficiency, and quality to build powerful AI applications on commodity GPUs and edge devices.
* [DeepSeek-V3](https://ollama.com/library/deepseek-v3): A strong Mixture-of-Experts (MoE) language model with 671B total parameters with 37B activated for each token.
* [OLMo 2](https://ollama.com/library/olmo2): a new family of 7B and 13B models trained on up to 5T tokens. These models are on par with or better than equivalently sized fully open models, and competitive with open-weight models such as Llama 3.1 on English academic benchmarks.

[2.10.6]
* Update ollama to 0.5.6
* [Full Changelog](https://github.com/open-webui/open-webui/releases/tag/v0.5.4)
* Fixed errors that would occur when running `ollama create` on Windows and when using absolute paths
* [@&#8203;steveberdy](https://github.com/steveberdy) made their first contribution in https://github.com/ollama/ollama/pull/8352

[2.10.7]
* Update ollama to 0.5.7
* [Full Changelog](https://github.com/open-webui/open-webui/releases/tag/v0.5.4)
* Fixed issue where using two `FROM` commands in `Modelfile`
* Support importing Command R and Command R+ architectures from safetensors
* [@&#8203;Gloryjaw](https://github.com/Gloryjaw) made their first contribution in https://github.com/ollama/ollama/pull/8438

[2.10.8]
* Update open-webui to 0.5.6
* [Full Changelog](https://github.com/open-webui/open-webui/releases/tag/v0.5.4)
* ** Effortful Reasoning Control for OpenAI Models**: Introduced the reasoning_effort parameter in chat controls for supported OpenAI models, enabling users to fine-tune how much cognitive effort a model dedicates to its responses, offering greater customization for complex queries and reasoning tasks.
* ** Chat Controls Loading UI Bug**: Resolved an issue where collapsible chat controls appeared as "loading," ensuring a smoother and more intuitive user experience for managing chat settings.
* ** Updated Ollama Model Creation**: Revamped the Ollama model creation method to align with their new JSON payload format, ensuring seamless compatibility and more efficient model setup workflows.

[2.10.9]
* Update open-webui to 0.5.7
* [Full Changelog](https://github.com/open-webui/open-webui/releases/tag/v0.5.4)
* ** Enhanced Internationalization (i18n)**: Refined and expanded translations for greater global accessibility and a smoother experience for international users.
* ** Connection Model ID Resolution**: Resolved an issue preventing model IDs from registering in connections.
* ** Prefix ID for Ollama Connections**: Fixed a bug where prefix IDs in Ollama connections were non-functional.
* ** Ollama Model Enable/Disable Functionality**: Addressed the issue of enable/disable toggles not working for Ollama base models.
* ** RBAC Permissions for Tools and Models**: Corrected incorrect Role-Based Access Control (RBAC) permissions for tools and models, ensuring that users now only access features according to their assigned privileges, enhancing security and role clarity.

[2.10.10]
* Update open-webui to 0.5.10
* [Full Changelog](https://github.com/open-webui/open-webui/releases/tag/v0.5.4)
* ** System Prompts Now Properly Templated via API**: Resolved an issue where system prompts were not being correctly processed when used through the API, ensuring template variables now function as expected.
* ** '<thinking>' Tag Display Issue Fixed**: Fixed a bug where the 'thinking' tag was disrupting content rendering, ensuring clean and accurate text display.
* ** Code Interpreter Stability with Custom Functions**: Addressed failures when using the Code Interpreter with certain custom functions like Anthropic, ensuring smoother execution and better compatibility.
* ** "Think" Tag Display Issue**: Resolved a bug where the "Think" tag was not functioning correctly, ensuring proper visualization of the model's reasoning process before delivering responses.
* ** Code Interpreter**: Models can now execute code in real time to refine their answers dynamically, running securely within a sandboxed browser environment using Pyodide. Perfect for calculations, data analysis, and AI-assisted coding tasks!
* ** Redesigned Chat Input UI**: Enjoy a sleeker and more intuitive message input with improved feature selection, making it easier than ever to toggle tools, enable search, and interact with AI seamlessly.
* ** Native Tool Calling Support (Experimental)**: Supported models can now call tools natively, reducing query latency and improving contextual responses. More enhancements coming soon!

[2.10.11]
* Update ollama to 0.5.11

[2.10.12]
* Update open-webui to 0.5.12
* [Full Changelog](https://github.com/open-webui/open-webui/releases/tag/v0.5.4)
* ** Multiple Tool Calls Support for Native Function Mode**: Functions now can call multiple tools within a single response, unlocking better automation and workflow flexibility when using native function calling.
* ** Playground Text Completion Restored**: Addressed an issue where text completion in the Playground was not functioning.
* ** Direct Connections Now Work for Regular Users**: Fixed a bug where users with the 'user' role couldn't establish direct API connections, enabling seamless model usage for all user tiers.
* ** Landing Page Input No Longer Lags with Long Text**: Improved input responsiveness on the landing page, ensuring fast and smooth typing experiences even when entering long messages.
* ** Parameter in Functions Fixed**: Fixed an issue where the reserved parameters wasnt recognized within functions, restoring full functionality for advanced task-based automation.
* ** Kokoro-JS TTS Support**: A new on-device, high-quality text-to-speech engine has been integrated, vastly improving voice generation qualityeverything runs directly in your browser.
* ** Jupyter Notebook Support in Code Interpreter**: Now, you can configure Code Interpreter to run Python code not only via Pyodide but also through Jupyter, offering a more robust coding environment for AI-driven computations and analysis.
* ** Direct API Connections for Private & Local Inference**: You can now connect Open WebUI to your private or localhost API inference endpoints. CORS must be enabled, but this unlocks direct, on-device AI infrastructure support.
* ** Advanced Domain Filtering for Web Search**: You can now specify which domains should be included or excluded from web searches, refining results for more relevant information retrieval.
* ** Improved Image Generation Metadata Handling**: Generated images now retain metadata for better organization and future retrieval.
* ** S3 Key Prefix Support**: Fine-grained control over S3 storage file structuring with configurable key prefixes.
* ** Support for Image-Only Messages**: Send messages containing only images, facilitating more visual-centric interactions.
* ** Updated Translations**: German, Spanish, Traditional Chinese, and Catalan translations updated for better multilingual support.
* ** OAuth Debug Logs & Username Claim Fixes**: Debug logs have been added for OAuth role and group management, with fixes ensuring proper OAuth username retrieval and claim handling.
* ** Citations Formatting & Toggle Fixes**: Inline citation toggles now function correctly, and citations with more than three sources are now fully visible when expanded.
* ** ComfyUI Maximum Seed Value Constraint Fixed**: The maximum allowed seed value for ComfyUI has been corrected, preventing unintended behavior.
* ** Connection Settings Stability**: Addressed connection settings issues that were causing instability when saving configurations.
* ** GGUF Model Upload Stability**: Fixed upload inconsistencies for GGUF models, ensuring reliable local model handling.
* ** Web Search Configuration Bug**: Fixed issues where web search filters and settings weren't correctly applied.
* ** User Settings Persistence Fix**: Ensured user-specific settings are correctly saved and applied across sessions.
* ** OpenID Username Retrieval Enhancement**: Usernames are now correctly picked up and assigned for OpenID Connect (OIDC) logins.

[2.10.13]
* Update open-webui to 0.5.14
* [Full Changelog](https://github.com/open-webui/open-webui/releases/tag/v0.5.4)
* ** Critical Import Error Resolved**: Fixed a circular import issue preventing 'override_static' from being correctly imported in 'open_webui.config', ensuring smooth system initialization and stability.
* ** Full Context Mode for Web Search**: Enable highly accurate web searches by utilizing full context modeideal for models with large context windows, ensuring more precise and insightful results.
* ** Optimized Asynchronous Web Search**: Web searches now load significantly faster with optimized async support, providing users with quicker, more efficient information retrieval.
* ** Auto Text Direction for RTL Languages**: Automatic text alignment based on language input, ensuring seamless conversation flow for Arabic, Hebrew, and other right-to-left scripts.
* ** Jupyter Notebook Support for Code Execution**: The "Run" button in code blocks can now use Jupyter for execution, offering a powerful, dynamic coding experience directly in the chat.
* ** Message Delete Confirmation Dialog**: Prevent accidental deletions with a new confirmation prompt before removing messages, adding an additional layer of security to your chat history.
* ** Download Button for SVG Diagrams**: SVG diagrams generated within chat can now be downloaded instantly, making it easier to save and share complex visual data.
* ** General UI/UX Improvements and Backend Stability**: A refined interface with smoother interactions, improved layouts, and backend stability enhancements for a more reliable, polished experience.
* ** Temporary Chat Message Continue Button Fixed**: The "Continue Response" button for temporary chats now works as expected, ensuring an uninterrupted conversation flow.
* ** Prompt Variable Update**: Deprecated square bracket '\[]' indicators for prompt variables; now requires double curly brackets '{{}}' for consistency and clarity.
* ** Stability Enhancements**: Error handling improved in chat history, ensuring smoother operations when reviewing previous messages.

[2.10.14]
* Update open-webui to 0.5.16
* [Full Changelog](https://github.com/open-webui/open-webui/releases/tag/v0.5.4)
* ** Web Search Retrieval Restored**: Resolved a critical issue that broke web search retrieval by reverting deduplication changes, ensuring complete and accurate search results once again.
* ** Full Context Mode for Local Document Search (RAG)**: Toggle full context mode from Admin Settings > Documents to inject entire document content into context, improving accuracy for models with large context windowsideal for deep context understanding.
* ** Smarter Web Search with Agentic Workflows**: Web searches now intelligently gather and refine multiple relevant terms, similar to RAG handling, delivering significantly better search results for more accurate information retrieval.
* ** Experimental Playwright Support for Web Loader**: Web content retrieval is taken to the next level with Playwright-powered scraping for enhanced accuracy in extracted web data.

[2.10.15]
* Update ollama to 0.5.12
* [Full Changelog](https://github.com/open-webui/open-webui/releases/tag/v0.5.4)
* [Perplexity R1 1776](https://ollama.com/library/r1-1776): A version of the DeepSeek-R1 model that has been post trained to remove its refusal to respond to some sensitive topics.
* The OpenAI-compatible API will now return `tool_calls` if the model called a tool
* Performance on certain Intel Xeon processors should now be restored
* Fixed permission denied issues after installing Ollama on Linux

[2.10.16]
* Update open-webui to 0.5.18
* [Full Changelog](https://github.com/open-webui/open-webui/releases/tag/v0.5.4)
* ** Open WebUI Now Works Over LAN in Insecure Context**: Resolved an issue preventing Open WebUI from functioning when accessed over a local network in an insecure context, ensuring seamless connectivity.
* ** UI Now Reflects Deleted Connections Instantly**: Fixed an issue where deleting a connection did not update the UI in real time, ensuring accurate system state visibility.
* ** Models Now Display Correctly with ENABLE_FORWARD_USER_INFO_HEADERS**: Addressed a bug where models were not visible when ENABLE_FORWARD_USER_INFO_HEADERS was set, restoring proper model listing.
* ** Instant Document Upload with Bypass Embedding & Retrieval**: Admins can now enable "Bypass Embedding & Retrieval" in Admin Settings > Documents, significantly speeding up document uploads and ensuring full document context is retained without chunking.

[2.10.17]
* Update ollama to 0.5.13
* [Full Changelog](https://github.com/open-webui/open-webui/releases/tag/v0.5.4)
* [Phi-4-Mini](https://ollama.com/library/phi4-mini): Phi-4-mini brings significant enhancements in multilingual support, reasoning, and mathematics, and now, the long-awaited function calling feature is finally supported.
* [Granite-3.2-Vision](https://ollama.com/library/granite3.2-vision): A compact and efficient vision-language model, specifically designed for visual document understanding, enabling automated content extraction from tables, charts, infographics, plots, diagrams, and more.
* [Command R7B Arabic](https://ollama.com/library/command-r7b-arabic): A new state-of-the-art version of the lightweight Command R7B model that excels in advanced Arabic language capabilities for enterprises in the Middle East and Northern Africa.
* The default context length can now be set with a new `OLLAMA_CONTEXT_LENGTH` environment variable. For example, to set the default context length to 8K, use: `OLLAMA_CONTEXT_LENGTH=8192 ollama serve`
* Ollama is now compiled for NVIDIA Blackwell

[2.10.18]
* Update open-webui to 0.5.19
* [Full Changelog](https://github.com/open-webui/open-webui/releases/tag/v0.5.4)
* ** Logit Bias Parameter Support**: Fine-tune conversation dynamics by adjusting the Logit Bias parameter directly in chat settings, giving you more control over model responses.
* ** Customizable Enter Behavior**: You can now configure Enter to send messages only when combined with Ctrl (Ctrl+Enter) via Settings > Interface, preventing accidental message sends.
* ** Collapsible Code Blocks**: Easily collapse long code blocks to declutter your chat, making it easier to focus on important details.
* ** Tag Selector in Model Selector**: Quickly find and categorize models with the new tag filtering system in the Model Selector, streamlining model discovery.
* ** Experimental Elasticsearch Vector DB Support**: Now supports Elasticsearch as a vector database, offering more flexibility for data retrieval in Retrieval-Augmented Generation (RAG) workflows.
* ** General Reliability Enhancements**: Various stability improvements across the WebUI, ensuring a smoother, more consistent experience.
* ** Updated Translations**: Refined multilingual support for better localization and accuracy across various languages.
* ** "Stream" Hook Activation**: Fixed an issue where the "Stream" hook only worked when globally enabled, ensuring reliable real-time filtering.
* ** LDAP Email Case Sensitivity**: Resolved an issue where LDAP login failed due to email case sensitivity mismatches, improving authentication reliability.
* ** WebSocket Chat Event Registration**: Fixed a bug preventing chat event listeners from being registered upon sign-in, ensuring real-time updates work properly.

[2.10.19]
* Update open-webui to 0.5.20
* [Full Changelog](https://github.com/open-webui/open-webui/releases/tag/v0.5.4)
* ** Toggle Code Execution On/Off**: You can now enable or disable code execution, providing more control over security, ensuring a safer and more customizable experience.
* ** Pinyin Keyboard Enter Key Now Works Properly**: Resolved an issue where the Enter key for Pinyin keyboards was not functioning as expected, ensuring seamless input for Chinese users.
* ** Web Manifest Loading Issue Fixed**: Addressed inconsistencies with 'site.webmanifest', guaranteeing proper loading and representation of the app across different browsers and devices.
* ** Non-Root Container Issue Resolved**: Fixed a critical issue where the UI failed to load correctly in non-root containers, ensuring reliable deployment in various environments.

