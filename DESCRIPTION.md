### About

Open WebUI is an extensible, feature-rich, and user-friendly self-hosted WebUI for various LLM runners, supported LLM runners include Ollama and OpenAI-compatible APIs.

### Features

* Intuitive Interface: Our chat interface takes inspiration from ChatGPT, ensuring a user-friendly experience.
* Responsive Design: Enjoy a seamless experience on both desktop and mobile devices.
* Swift Responsiveness: Enjoy fast and responsive performance.
* Effortless Setup: Install seamlessly using Docker or Kubernetes (kubectl, kustomize or helm) for a hassle-free experience.
* Code Syntax Highlighting: Enjoy enhanced code readability with our syntax highlighting feature.
* Full Markdown and LaTeX Support: Elevate your LLM experience with comprehensive Markdown and LaTeX capabilities for enriched interaction.
* Local RAG Integration
* Web Browsing Capability
* Prompt Preset Support
* RLHF Annotation
* Conversation Tagging
* Download/Delete Models
* GGUF File Model Creation

