FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code
WORKDIR /app/code

# install python 3.11
RUN apt-get update && \
    add-apt-repository -y ppa:deadsnakes && \
    apt-get install -y --no-install-recommends python3.11 python3.11-venv python3.11-dev python3.11-distutils && \
    rm -rf /usr/bin/python3 && ln -s /usr/bin/python3.11 /usr/bin/python3 && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# https://github.com/open-webui/open-webui/blob/main/Dockerfile#L61
RUN apt-get update && \
    apt-get install -y pandoc netcat-openbsd && \
    rm -rf /var/cache/apt /var/lib/apt/lists

# renovate: datasource=github-releases depName=open-webui/open-webui versioning=semver extractVersion=^v(?<version>.+)$
ARG OPENWEBUI_VERSION=0.5.20

RUN curl -L https://github.com/open-webui/open-webui/archive/refs/tags/v${OPENWEBUI_VERSION}.tar.gz | tar xz --strip-components 1 -C /app/code

# renovate: datasource=github-releases depName=ollama/ollama versioning=semver extractVersion=^v(?<version>.+)$
ARG OLLAMA_VERSION=0.5.13

RUN curl -L https://github.com/ollama/ollama/releases/download/v${OLLAMA_VERSION}/ollama-linux-amd64.tgz | tar xz -C /usr && \
    chmod +x /usr/bin/ollama

ENV NODE_OPTIONS="--max-old-space-size=4096"
ARG NODE_VERSION=20.18.1
RUN mkdir -p /usr/local/node-$NODE_VERSION && \
    curl -L https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION-linux-x64.tar.gz | tar zxf - --strip-components 1 -C /usr/local/node-$NODE_VERSION
ENV PATH /usr/local/node-$NODE_VERSION/bin:$PATH

RUN npm ci && \
    npm run build

WORKDIR /app/code/backend
# https://github.com/open-webui/open-webui/blob/main/Dockerfile#L121
RUN pip3 install uv
RUN pip3 install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cpu --no-cache-dir
RUN uv pip install --system -r requirements.txt --no-cache-dir

WORKDIR /app/code

RUN python3 -W ignore::RuntimeWarning -m nltk.downloader -d "/usr/share/nltk_data" punkt_tab

# backend/config.py copies favicon.png into static. static does not symlinked files, so we have to symlink the whole thing
RUN mv /app/code/backend/open_webui/static /app/code/backend/open_webui/static.orig && \
    ln -s /run/openwebui/static /app/code/backend/open_webui/static

# add supervisor configs
ADD supervisor/* /etc/supervisor/conf.d/
RUN ln -sf /run/openwebui/supervisord.log /var/log/supervisor/supervisord.log

COPY env.sh.template start.sh /app/pkg/

CMD ["/app/pkg/start.sh"]
