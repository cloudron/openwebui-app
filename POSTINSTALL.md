This app is pre-setup with an admin account. The initial credentials are:

**Email**: admin@cloudron.local<br/>
**Username**: admin<br/>
**Password**: changeme123<br/>

<sso>
Cloudron users get the `user` role by default. You can give the user `admin` role inside the admin dashboard.
</sso>

