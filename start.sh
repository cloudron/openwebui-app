#!/bin/bash
set -eu

echo "=> Ensure directories"
mkdir -p /app/data/ /run/openwebui/{hf-cache,static}

if [[ ! -f /app/data/.secret_key ]]; then
    echo "=> Creating WebUI secret key"
    openssl rand -base64 42 > /app/data/.secret_key
fi

echo "=> Copying static files"
cp -R /app/code/backend/open_webui/static.orig/. /run/openwebui/static

echo "=> Loading configuration"
export WEBUI_SECRET_KEY="$(cat /app/data/.secret_key)"

# export DATABASE_URL=${CLOUDRON_POSTGRESQL_URL}
# our URL has postgres:// but not postgresql:// !!
export DATABASE_URL="postgresql://${CLOUDRON_POSTGRESQL_USERNAME}:${CLOUDRON_POSTGRESQL_PASSWORD}@${CLOUDRON_POSTGRESQL_HOST}:5432/${CLOUDRON_POSTGRESQL_DATABASE}"

[[ ! -f /app/data/env.sh ]] && cp /app/pkg/env.sh.template /app/data/env.sh

# some defaults
export OLLAMA_BASE_URL="http://127.0.0.1:11434"
export OLLAMA_MODELS=/app/data/ollama-home/models
export NLTK_DATA=/app/data/nltk # https://github.com/nltk/nltk used by open-web-ui

# user overrides
source /app/data/env.sh

if [[ "${OLLAMA_ENABLED:-true}" ]]; then
    echo "=> Enabling local ollama"
    mkdir -p "${OLLAMA_MODELS}" "${NLTK_DATA}"
fi

echo "=> Setting permissions"
chown -R cloudron:cloudron /app/data /run/openwebui

wait_for_openwebui() {
    # Wait for app to come up
    sleep 5
    while ! curl --fail -s http://localhost:8080 > /dev/null; do
        echo "=> Waiting for app to come up"
        sleep 1
    done
}

db_initialized=$(psql -qtA ${CLOUDRON_POSTGRESQL_URL} -c "SELECT EXISTS (SELECT 1 FROM pg_class WHERE relname = 'user')")

if [[ "${db_initialized}" == "f" ]]; then
    echo "==> Initializing database on first run"
    ( cd /app/code/backend && HF_HOME=/run/openwebui/hf-cache ENV=prod DATA_DIR=/app/data gosu cloudron:cloudron uvicorn open_webui.main:app --host 0.0.0.0  --port 8080 --forwarded-allow-ips '*' ) &
    pid=$!

    wait_for_openwebui

    echo "==> Creating admin user"
    curl -s -X POST -H 'content-type: application/json' -d '{"name":"admin","email":"admin@cloudron.local","password":"changeme123"}' "http://localhost:8080/api/v1/auths/signup"
    sleep 3

    echo "==> Stopping after admin creation"
    kill -SIGTERM -${pid} # this kills the process group
fi

if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    echo "=> Update OIDC config"
    export ENABLE_OAUTH_SIGNUP=true
    export ENABLE_LOGIN_FORM=true
    export ENABLE_SIGNUP=false
    export OAUTH_MERGE_ACCOUNTS_BY_EMAIL=true
    export OAUTH_CLIENT_ID=${CLOUDRON_OIDC_CLIENT_ID}
    export OAUTH_CLIENT_SECRET=${CLOUDRON_OIDC_CLIENT_SECRET}
    export OPENID_PROVIDER_URL="${CLOUDRON_OIDC_ISSUER}/.well-known/openid-configuration"
    export OPENID_REDIRECT_URI="${CLOUDRON_APP_ORIGIN}/oauth/oidc/callback"
    export OAUTH_SCOPES="openid email profile"
    export OAUTH_PROVIDER_NAME="${CLOUDRON_OIDC_PROVIDER_NAME:-Cloudron}"
    export OAUTH_USERNAME_CLAIM="preferred_username"
    export OAUTH_PICTURE_CLAIM="picture"
    export OAUTH_EMAIL_CLAIM="email"
fi

echo "==> Starting supervisor"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i OpenWebUI

