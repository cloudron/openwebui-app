#!/usr/bin/env node

/* jshint esversion: 8 */
/* jslint node:true */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME, PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 50000;

    let browser, app;
    const adminUsername = 'admin';
    const adminEmail = 'admin@cloudron.local';
    const adminPassword = 'changeme123';
    const EMAIL = process.env.EMAIL;
    const USERNAME = process.env.USERNAME;
    const PASSWORD = process.env.PASSWORD;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 }).addArguments('--disable-gpu');
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function sleep(millis) {
        return new Promise(resolve => setTimeout(resolve, millis));
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TIMEOUT);
    }

    async function dismissPopup() {
        await waitForElement(By.xpath('//button[contains(., "Okay")]'));
        await browser.findElement(By.xpath('//button[contains(., "Okay")]')).click();
        await browser.sleep(5000);
    }

    async function login(email, password, dismiss) {
        await browser.get(`https://${app.fqdn}/auth`);
        await waitForElement(By.xpath('//input[@type="password"]'));
        await browser.findElement(By.xpath('//input[@type="email"]')).sendKeys(email);
        await browser.findElement(By.xpath('//input[@type="password"]')).sendKeys(password);
        await browser.findElement(By.xpath('//button[contains(text(), "Sign")]')).click();
        await browser.sleep(2000);
        if (dismiss) await dismissPopup();
        await waitForElement(By.xpath('//div[contains(., "admin") or contains(text(), "admin")]'));
    }

    async function loginOIDC(username, password, alreadyAuthenticated = true, dismiss = true) {
        await browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/auth`);
        await browser.sleep(2000);

        await waitForElement(By.xpath('//button[contains(., "Continue with")]'));
        await browser.findElement(By.xpath('//button[contains(., "Continue with")]')).click();
        await browser.sleep(2000);

        if (!alreadyAuthenticated) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
        }
        if (dismiss) await dismissPopup();
        await waitForElement(By.xpath(`//div[contains(., "${username}") or contains(., "${EMAIL}") or contains(text(), "admin")]`));
    }

    async function logout() {
        await browser.manage().deleteAllCookies();
        await browser.quit();
        browser = null;
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 }).addArguments('--disable-gpu');
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        chromeOptions.addArguments(`--user-data-dir=${await fs.promises.mkdtemp('/tmp/test-')}`); // --profile-directory=Default
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
    }

    async function getMainPage() {
        await browser.get(`https://${app.fqdn}/`);
        await waitForElement(By.xpath('//div[contains(., "Hello")]'));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', async function () {
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        await browser.sleep(60000);
    });

    it('can get app information', getAppInfo);
    it('can admin login', login.bind(null, adminEmail, adminPassword, true));
    it('can get the main page', getMainPage);

    it('can logout', logout.bind(null, adminUsername));

    it('can login via OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, false, false));
    it('can get the main page', getMainPage);
    it('can logout', logout.bind(null, USERNAME));

    it('can restart app', async function () {
        execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS);
        await sleep(60000);
    });

    it('can admin login', login.bind(null, adminEmail, adminPassword, false));
    it('can get the main page', getMainPage);
    it('can logout', logout.bind(null, adminUsername));

    it('can login via OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, false, false));
    it('can get the main page', getMainPage);
    it('can logout', logout.bind(null, USERNAME));

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async function () {
        await browser.get('about:blank');
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
        await sleep(60000);
    });

    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, adminEmail, adminPassword, false));
    it('can get the main page', getMainPage);
    it('can logout', logout.bind(null, adminUsername));

    it('can login via OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, false, false));
    it('can get the main page', getMainPage);
    it('can logout', logout.bind(null, USERNAME));


    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });
    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, adminEmail, adminPassword, false));
    it('can get the main page', getMainPage);
    it('can logout', logout.bind(null, adminUsername));

    it('can login via OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, false, false));
    it('can get the main page', getMainPage);
    it('can logout', logout.bind(null, USERNAME));

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app for update', async function () {
        execSync(`cloudron install --appstore-id com.openwebui.cloudronapp --location ${LOCATION}`, EXEC_ARGS);
        await browser.sleep(60000);
    });

    it('can get app information', getAppInfo);
    it('can admin login', login.bind(null, adminEmail, adminPassword, false)); // maybe the popup is some local cookie?
    it('can get the main page', getMainPage);
    it('can logout', logout.bind(null, adminUsername));
    it('can login via OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, false, false));
    it('can get the main page', getMainPage);
    it('can logout', logout.bind(null, USERNAME));
    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });

    it('can admin login', login.bind(null, adminEmail, adminPassword, false));
    it('can get the main page', getMainPage);
    it('can logout', logout.bind(null, adminUsername));
    it('can login via OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, false, false));
    it('can get the main page', getMainPage);
    it('can logout', logout.bind(null, USERNAME));

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
